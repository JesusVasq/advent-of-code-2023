CC = gcc
CFLAGS = -Wall -Wextra -std=c11

all: day11

day11: main.c
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -f day11